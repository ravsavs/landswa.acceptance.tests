package com.landswa.poms;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class AssignApplicantCustomerPage extends BasePage
{
    private WebDriver _driver;
    ApplicantSearch applicantSearch;

    public AssignApplicantCustomerPage(WebDriver driver) throws Exception 
    {
    	super(driver);
        _driver = driver;
        applicantSearch = new ApplicantSearch(_driver);
    }
	@Override
	protected By IsPageLoadedBy() {
		return By.xpath("//h1[contains(text(),'Assign Applicant/Customer to the New Case')]");
	}

    public AssignApplicantCustomerPage SearchAnApplicantWithName(String name) throws Exception
    {
        applicantSearch.EnterFirstName(name)
            .clickApplyButton();
        return this;
    }

    public AssignApplicantCustomerPage SelectTheApplicantFromSearchResultWithName(String name)
    {
        applicantSearch.SelectApplicantFromResults(name);
        return this;
    }

    public ApplicantDetailsMileStone Continue() throws Exception
    {
        applicantSearch.clickContinueButton();
        return new ApplicantDetailsMileStone(_driver);
    }

}

class ApplicantSearch extends BasePage
{
    protected String staticPageHeading = "//h2[contains(text(),'Search for Applicant')]";
    protected String firstNameField = "//*[text()='First Name']/../..//input";
    protected String disabledApplyButton = "//div[@class='Button---disabled_btn_glass']/following-sibling::button[text()='Apply']";
    protected String enabledApplyButton = "//button[contains(text(),'Apply')]";

    protected String searchResultText = "//h2[text()='Search Results']";
    protected String searchResultFirstColumnHeading = "//div[contains(text(),'First Name')]";

    protected String disabledContinueButton = "//div[@class='Button---disabled_btn_glass']/following-sibling::button[text()='Continue']";
    protected String enabledContinueButton = "//button[contains(text(),'Continue')]";

    public ApplicantSearch(WebDriver driver) throws Exception
    {
    	 super(driver);
    }

	@Override
	protected By IsPageLoadedBy() {
		return By.xpath("//h2[contains(text(),'Search for Applicant')]");
	}
	
    public ApplicantSearch EnterFirstName(String firstName) throws Exception
    {
        GetElementByXpath(firstNameField).sendKeys(firstName);
        return this;
    }

    public ApplicantSearch clickApplyButton() throws Exception
    {
        GetElementByXpath(staticPageHeading).click();
        GetElementByXpath(enabledApplyButton).click();
        return this;
    }

    public Boolean IsApplicantSearchResultEmpty()
    {
        WebElement ele;
        try
        {
            ele = GetElementByXpath(searchResultFirstColumnHeading);
        }
        catch(Exception e)
        {
            System.out.println("Search Result is empty");
            return false;
        }
        return true;
    }

    public Boolean ApplicantPresentInSearchResult(String name)
    {
        WebElement ele;
        try
        {
            ele = GetElementByXpath("//p[contains(text(),'"+name+"')]");
        }
        catch (Exception e)
        {
            System.out.println("Search Result does not contain the name - " + name);
            return false;
        }
        return true;
    }

    public void SelectApplicantFromResults(String name)
    {
        
        WebElement ele = null;
        try
        {
            ele = GetElementByXpath("//p[contains(text(),'"+name+"')]/../../td[1]/div");
        }
        catch (Exception e)
        {
            System.out.println("Search Result does not contain the name - " + name);
        }
        ele.click();
    }

    public void clickContinueButton() throws Exception
    {
        GetElementByXpath(staticPageHeading).click();
        GetElementByXpath(enabledContinueButton).click();
    }

}

class CustomerSearch extends BasePage
{
    public CustomerSearch(WebDriver driver) throws Exception 
    {
    	super(driver);
    }
       
	@Override
	protected By IsPageLoadedBy() {
		return By.xpath("//h1[contains(text(),'//h2[contains(text(),'Search for Customer')]");
	}
}
