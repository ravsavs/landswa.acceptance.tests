package com.landswa.poms;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class ApplicantDetailsMileStone extends BasePage {
	
    private WebDriver _driver;
    protected String staticPageElement = "//h2/a[contains(text(), 'Applicant Details')]";
    protected String continueButton = "//button[text()='Continue']";

    public ApplicantDetailsMileStone(WebDriver driver) throws Exception
    {
    	super(driver);
        _driver = driver;
        GetElementByXpath(staticPageElement);
    }

    public RequestDetailsMileStone ClickContinueButton() throws Exception
    {
        GetElementByXpath(continueButton).click();
        return new RequestDetailsMileStone(_driver);
    }

	@Override
	protected org.openqa.selenium.By IsPageLoadedBy() {
		return By.xpath("//a[text()='Applicant Details']/span[text()='Current Step']");
	}

}
