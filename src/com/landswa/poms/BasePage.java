package com.landswa.poms;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.landswa.enums.User;

public abstract class BasePage
{
	User userProfile;
	Boolean pageLoaded;
	
    public User getUserProfile() { return userProfile; }
    public void setUserProfile(User userProfile) { this.userProfile = userProfile; }

    private static WebDriver _driver;
    protected static WebDriverWait Wait = null;       
    protected abstract By IsPageLoadedBy();

    protected BasePage(WebDriver driver) throws Exception
    {
        SetWait(driver, 60);
        if (IsDocumentReady())
            PageFactory.initElements(driver, this);
        else
            throw new Exception("Page not loaded correctly(BasePage-constructor)");
        _driver = driver;
    }

    public Boolean IsPageLoaded()
    {
        try
        {
            if(IsDocumentReady())
                Wait.until(ExpectedConditions.visibilityOfElementLocated(IsPageLoadedBy()));
            return true;
        }
        catch (Exception e)
        {
            return false;
        }
    }
    
    public static void SetWait(WebDriver driver, int WaitForElementInSeconds)
    {
        Wait = new WebDriverWait(driver, 60000);
    }
    
    private Boolean IsDocumentReady()
    {
        return Wait.until((driver) ->
        {
            Boolean isDocumentReady = (Boolean)((JavascriptExecutor)driver).
                executeScript("return document.readyState").
                toString().
                equals("complete");
            return isDocumentReady;
        });
    }
    
    private WebElement GetElement(By by) throws Exception
    {
        Thread.sleep(1500);
        Boolean pageLoad = IsDocumentReady();
        if (pageLoad)
        {
            Wait.until(ExpectedConditions.elementToBeClickable(by));
            Wait.until(ExpectedConditions.visibilityOfElementLocated(by));
            Wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(by));
            return (WebElement) Wait.until((d) -> d.findElement(by));
        }
        else
            return null;
    }

    protected WebElement GetElementByText(String text) throws Exception
    {
        return GetElement(By.xpath("//*[text()='{"+text+"}']"));
    }

    protected WebElement GetElementById(String id) throws Exception
    {
        return GetElement(By.id(id));
    }

    protected WebElement GetElementBySelector(String selector) throws Exception
    {
        return GetElement(By.cssSelector(selector));
    }

    protected WebElement GetElementByXpath(String selector) throws Exception
    {
        return GetElement(By.xpath(selector));
    }

    protected List<WebElement> GetElementsByXPath(String selector)
    {
    	ArrayList<WebElement> ele = (ArrayList<WebElement>) 
    			Wait.until((d)->d.findElements(By.xpath(selector)));

        return ele;
    }
}
