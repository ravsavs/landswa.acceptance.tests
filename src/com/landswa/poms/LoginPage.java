package com.landswa.poms;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.landswa.enums.User;
import com.landswa.helper.Constants;

public class LoginPage extends BasePage {
	WebDriver _driver = null;

    public WebElement Username = GetElementByXpath("//input[@id='un']");
    public WebElement Password = GetElementByXpath("//input[@id='pw']");
    public WebElement SubmitButton = GetElementByXpath("//input[@type='submit']");

    public LoginPage(WebDriver driver) throws Exception
    {
    	super(driver);
        _driver = driver;
    }

	@Override
	protected By IsPageLoadedBy() {
		return By.xpath("//title[contains(text(),'Appian for WA Department of Lands (TEST)')]");
	}
	
    public LoginPage EnterUsername(String username, User user)
    {
        userProfile = user;
        Username.sendKeys(username);
        return this;
    }

    public LoginPage EnterPassword(String password)
    {
        Password.sendKeys(password);
        return this;
    }

    public BasePage ClickLoginButton() throws Exception
    {
        SubmitButton.click();
        switch (userProfile)
        {
            case Officer:
                return new MyDashboardPage(_driver);
            case Manager:
                return new TeamDashboardPage(_driver);
            default:
                break;
        }
        return null;
    }

}
