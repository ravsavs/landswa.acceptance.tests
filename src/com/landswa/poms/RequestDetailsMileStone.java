package com.landswa.poms;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class RequestDetailsMileStone extends BasePage {
	
    private WebDriver _driver;
    protected String staticPageElement = "//h2[contains(text(), 'Request Details')]";
    protected String continueButton = "//button[text()='Continue']";
    protected String genrealRequestType = "//label[text()='General Crown land request']";
    protected String crownLandRequestType = "//label[text()='Crown land request from Local Government, Management Body or State Government Agency']";
    protected String eventRequestRequestType = "//label[text()='Request for access to Crown land for an event']";
    protected String categoryDropdown = "//div[text()='Select one item...']";
    protected String descriptionTextArea = "//textarea";

    public RequestDetailsMileStone(WebDriver driver) throws Exception
    {
    	super(driver);
        _driver = driver;
        GetElementByXpath(staticPageElement).click();
    }
    

	@Override
	protected By IsPageLoadedBy() {
		return By.xpath("//a[text()='Request Details']/span[text()='Current Step']");
	}

    public RequestDetailsMileStone SelectGeneralRequestType() throws Exception
    {
        GetElementByXpath(genrealRequestType).click();
        return this;
    }

    public RequestDetailsMileStone ClickOnCategoryDropDown() throws Exception
    {
        GetElementByXpath(categoryDropdown).click();
        return this;
    }

    public RequestDetailsMileStone SelectFromDopDown(String name) throws Exception
    {
        GetElementByXpath("//*[text()='"+name+"']").click();
        return this;
    }

    public RequestDetailsMileStone EnterDescription(String desc) throws Exception
    {
        GetElementByXpath(descriptionTextArea).sendKeys(desc);
        return this;
    }

    public LandDetailsMileStone ClickContinueButton() throws Exception
    {
        GetElementByXpath(continueButton).click();
        return new LandDetailsMileStone(_driver);
    }


}
