package com.landswa.poms;

import org.joda.time.DateTime;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class TermsAndConditionsMileStone extends BasePage {
    private WebDriver _driver;
    protected String staticPageElement = "//h2[contains(text(), 'Terms and Conditions')]";
    protected String submitButton = "//button[text()='Submit']";
    protected String termsAndConditionsCheckbox = "//label[text()=' Applicant has read and agreed with the above Terms and Conditions *']";
    protected String signedCheckbox = "//label[text()='Signed by Applicant *']";
    protected String dateReceived = "//label[text()='Date Received']/../../div[2]//input";
    protected String dateSigned = "//label[text()='Date Signed']/../../div[2]//input";
    protected String currentDate = DateTime.now().toString("dd/MM/yyyy"); 

    public TermsAndConditionsMileStone(WebDriver driver) throws Exception
    {
    	super(driver);
        _driver = driver;
        GetElementByXpath(staticPageElement).click();
    }

	@Override
	protected org.openqa.selenium.By IsPageLoadedBy() {
		return By.xpath("//a[text()='Terms and Conditions']/span[text()='Current Step']");
	}
    public TermsAndConditionsMileStone ClickTermsAndConditionsCheckbox() throws Exception
    {
        GetElementByXpath(termsAndConditionsCheckbox).click();
        return this;
    }

    public TermsAndConditionsMileStone ClickSignedCheckbox() throws Exception
    {
        GetElementByXpath(signedCheckbox).click();
        return this;
    }

    public TermsAndConditionsMileStone EnterDateReceived() throws Exception
    {
        GetElementByXpath(dateReceived).sendKeys(currentDate);
        return this;
    }

    public TermsAndConditionsMileStone EnterDatesigned() throws Exception
    {
        GetElementByXpath(dateSigned).sendKeys(currentDate);
        return this;
    }

    public SubmissionMileStone ClickSubmitButton() throws Exception
    {
        GetElementByXpath(staticPageElement).click();
        GetElementByXpath(submitButton).click();
        return new SubmissionMileStone(_driver);
    }


}
