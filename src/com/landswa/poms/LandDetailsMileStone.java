package com.landswa.poms;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class LandDetailsMileStone extends BasePage {
	
    private WebDriver _driver;
    protected String staticPageElement = "//h2[contains(text(), 'Land Details')]";
    protected String addLandRecordImg = "//img[@aria-label='Add Land Record']";
    protected String localGovtAuthority = "//span[text()='Local Government Authority']/../..//input";
    protected String selectedLGA = "//span[@class='PickerTokenWidget---label']";
    protected String addButton = "//button[text()='ADD']";
    protected String continueButton = "//button[text()='Continue']";
    protected String selectedLGATableCell = "//tbody/tr/td[9]";

    protected String LGAName = null;

    public LandDetailsMileStone(WebDriver driver) throws Exception 
    {
    	super(driver);
        _driver = driver;
        GetElementByXpath(staticPageElement).click();
    }

	@Override
	protected org.openqa.selenium.By IsPageLoadedBy() {
		return By.xpath("//a[text()='Land Details']/span[text()='Current Step']");
	}
	
    public LandDetailsMileStone AddLandRecordForLGA(String LGAName) throws Exception
    {
        ClickOnAddLandRecordImage();
        AddLGA(LGAName);
        ClickAddButton();
        return this;
    }

    public LandDetailsMileStone ClickOnAddLandRecordImage() throws Exception
    {
        GetElementByXpath(addLandRecordImg).click();
        return this;
    }

    public LandDetailsMileStone AddLGA(String LGAName) throws Exception
    {
        WebElement ele = GetElementByXpath(localGovtAuthority);
        ele.sendKeys(LGAName);
        Thread.sleep(1000);
        ele.sendKeys(Keys.ARROW_DOWN);
        Thread.sleep(1000);
        ele.sendKeys(Keys.ENTER);
        this.LGAName = GetElementByXpath(selectedLGA).getText();
        return this;
    }

    public LandDetailsMileStone ClickAddButton() throws Exception
    {
        GetElementByXpath(addButton).click();
        return this;
    }

    public Boolean IsLandRecordAdded() throws Exception
    {
        return GetElementByXpath(selectedLGATableCell).getText() == LGAName;
    }

    public ConsultationMileStone ClickContinueButton() throws Exception
    {
        GetElementByXpath(continueButton).click();
        return new ConsultationMileStone(_driver);
    }


}
