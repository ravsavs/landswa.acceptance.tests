package com.landswa.poms;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class ReviewMileStone extends BasePage {
    private WebDriver _driver;
    protected String staticPageElement = "//h2[contains(text(), 'Review')]";
    protected String checkbox = "//input/..";
    protected String continueButton = "//button[text()='Continue']";

    public ReviewMileStone(WebDriver driver) throws Exception
    {
    	super(driver);
        _driver = driver;
        GetElementByXpath(staticPageElement).click();
    }
    
	@Override
	protected org.openqa.selenium.By IsPageLoadedBy() {
		return By.xpath("//a[text()='Review']/span[text()='Current Step']");
	}

    public ReviewMileStone ClickCheckBox() throws Exception
    {
        GetElementByXpath(checkbox).click();
        return this;
    }

    public TermsAndConditionsMileStone ClickContinueButton() throws Exception
    {
        GetElementByXpath(continueButton).click();
        return new TermsAndConditionsMileStone(_driver);
    }
}
