package com.landswa.poms;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class MyDashboardPage extends BasePage {
	
	WebDriver _driver;
	
	public MyDashboardPage(WebDriver driver) throws Exception
	{
		super(driver);
		_driver = driver;
	}
    
    
	@Override
	protected By IsPageLoadedBy() {
		return By.xpath("//title[contains(text(),'My Dashboard - iWMS DoL Officer Site')]");
	}
	
    public Boolean IsPageLoadComplete()
    {
        return IsPageLoaded();
    }

    public Boolean IsOfficerNameDisplayed(String name) throws Exception
    {
        WebElement NameElement = GetElementByXpath("//span[contains(text(),'"+name+"')]");
        return NameElement.isDisplayed();
    }

    public AssignApplicantCustomerPage ClickCreateNewCaseButon() throws Exception
    {
        WebElement ele = null;
        try
        {
            ele = GetElementByXpath("//button[contains(text(),'CREATE NEW CASE')]");
        }
        catch(Exception e)
        {
            System.out.println("'CREATE NEW CASE' button is not available to this user");
            System.out.println(e.getMessage());
        }
        ele.click();
        return new AssignApplicantCustomerPage(_driver);
    }

}
