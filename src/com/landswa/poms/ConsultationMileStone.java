package com.landswa.poms;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import com.landswa.enums.Decision;

public class ConsultationMileStone extends BasePage {
	
    private WebDriver _driver;
    protected String staticPageElement = "//h2[contains(text(), 'Consultation Details')]";
    protected String checkbox = "//tbody/tr/td[1]/div";
    protected String LGABeenConsultedRadioButton = "//div[@role='radiogroup']/div[1]";
    protected String updateButton = "//button[text()='UPDATE']";
    protected String continueButton = "//button[text()='Continue']";

    public ConsultationMileStone(WebDriver driver) throws Exception
    {
    	super(driver);
        _driver = driver;
        GetElementByXpath(staticPageElement).click();
    }

	@Override
	protected By IsPageLoadedBy() {
		return By.xpath("//a[text()='Consultation']/span[text()='Current Step']");
	}
	
    public ConsultationMileStone ClickLGACheckboxToConsult() throws Exception
    {
        GetElementByXpath(checkbox).click();
        return this;
    }

    public ConsultationMileStone HasLGABeenConsultedRadioButtonResponse(Decision desc) throws Exception
    {
        switch(desc)
        {
            case Yes:
                GetElementByXpath("//div[@role='radiogroup']/div[1]").click();
                break;
            case No:
                GetElementByXpath("//div[@role='radiogroup']/div[2]").click();
                break;
            case NotApplicable:
                GetElementByXpath("//div[@role='radiogroup']/div[3]").click();
                break;
        }
        return this;
    }

    public ConsultationMileStone ClickUpdateButton() throws Exception
    {
        GetElementByXpath(updateButton).click();
        return this;
    }

    public AdditionalInformationMileStone ClickContinueButton() throws Exception
    {
        GetElementByXpath(continueButton).click();
        return new AdditionalInformationMileStone(_driver);
    }

}
