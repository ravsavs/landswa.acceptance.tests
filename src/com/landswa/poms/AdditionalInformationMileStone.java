package com.landswa.poms;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class AdditionalInformationMileStone extends BasePage {

        private WebDriver _driver;
        protected String staticPageElement = "//h2[contains(text(), 'Additional Information')]";
        protected String continueButton = "//button[text()='Continue']";

        public AdditionalInformationMileStone(WebDriver driver) throws Exception
        {
        	super(driver);
            _driver = driver;
            GetElementByXpath(staticPageElement).click();
        }

		@Override
		protected org.openqa.selenium.By IsPageLoadedBy() {
			return By.xpath("//a[text()='Additional Information']/span[text()='Current Step']");
		}
		
        public ReviewMileStone ClickContinueButton() throws Exception
        {
            GetElementByXpath(continueButton).click();
            return new ReviewMileStone(_driver);

        }

}
