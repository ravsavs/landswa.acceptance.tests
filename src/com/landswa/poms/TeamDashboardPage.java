package com.landswa.poms;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class TeamDashboardPage extends BasePage {

	WebDriver _driver;
	
	public TeamDashboardPage(WebDriver driver) throws Exception
	{
		super(driver);
		_driver = driver;
	}

	@Override
	protected By IsPageLoadedBy() {
		return By.xpath("//title[contains(text(),'Team Dashboard - iWMS DoL Officer Site')]");
	}
	
    public Boolean IsPageLoadComplete()
    {
        return IsPageLoaded();
    }

    public Boolean IsManagerNameDisplayed(String name) throws Exception
    {
        WebElement NameElement = GetElementByXpath("//span[contains(text(),'"+name+"')]");
        return NameElement.isDisplayed();
    }
}
