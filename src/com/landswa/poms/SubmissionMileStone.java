package com.landswa.poms;

import org.joda.time.DateTime;
import org.openqa.selenium.WebDriver;

import com.landswa.enums.ContactMethod;
import com.landswa.enums.Decision;

public class SubmissionMileStone extends BasePage {
	
     private WebDriver _driver;
     protected String staticPageElement = "//h2[contains(text(), 'Submission Receipt')]";
     protected String methodOfContact = "//span[text()='Method of contact *']/../../div[2]";
     protected String printAndPostDocumentSentDate = "//input[contains(@class,'DatePicker')]";
     protected String readyToEmailCheckbox = "//label[text()='* Confirm ready to email']";
     protected String doneButton = "//button[text()='DONE']";

     public SubmissionMileStone(WebDriver driver) throws Exception
     {
    	 super(driver);
         _driver = driver;
         GetElementByXpath(staticPageElement).click();
     }
 	@Override
 	protected org.openqa.selenium.By IsPageLoadedBy() {
 		return org.openqa.selenium.By.xpath("//a[text()='Submission']/span[text()='Current Step']");
 	}

     public SubmissionMileStone SendCaseSummaryToApplicantRadioButton(Decision decision) throws Exception
     {
         switch(decision)
         {
             case Yes:
                 GetElementByXpath("//label[text()='Yes']").click();
                 break;
             case No:
                 GetElementByXpath("//label[text()='No']").click();
                 break;
         } 
         return this;
     }

     public SubmissionMileStone SelectMethodOfContact(ContactMethod contactMethod) throws Exception
     {
         GetElementByXpath(methodOfContact).click();
         switch(contactMethod)
         {
             case Email:
                 GetElementByXpath("//*[text()='Email']").click();
                 break;
             case PrintAndPost:
                 GetElementByXpath("//*[text()='Print & Post']").click();
                 break;
             case BothEmailAndPrintAndPost:
                 GetElementByXpath("//*[text()='Both Email and Print & Post']").click();
                 break;
         }
         return this;
     }
     
     public SubmissionMileStone AnyOtherDocumentsToSend(Decision decision) throws Exception
     {
         switch(decision)
         {
             case Yes:
                 GetElementByXpath("//span[contains(text(),'Are there other documents to send to')]/../../../../div[2]//label[text()='Yes']").click();
                 break;
             case No:
                 GetElementByXpath("//span[contains(text(),'Are there other documents to send to')]/../../../../div[2]//label[text()='No']").click();
                 break;
             default:
               	 break;
         }
         return this;
     }

     public SubmissionMileStone EnterTheDatePrintAndPostDocumentWereSent() throws Exception
     {
         String currentDate = DateTime.now().toString("dd/MM/yyyy");
         GetElementByXpath(printAndPostDocumentSentDate).sendKeys(currentDate);
         return this;
     }

     public SubmissionMileStone ClickReadyToEmailConfirmationCheckbox() throws Exception
     {
         GetElementByXpath(readyToEmailCheckbox).click();
         return this;
     }

     public void ClickDoneButton() throws Exception
     {
         GetElementByXpath(doneButton).click();
     }
}
