package com.landswa.acceptance.tests;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;


public class LoginTests extends SetupTeardown{
    WebDriver _driver = null;

	public void runSelenium()
	{
		TestSuiteSetup();
		SetUp();
		Driver.get("http://toolsqa.wpengine.com/automation-practice-form/");
		TestTeardown();
	}
	
	int num1, num2;
	public void setFirstNumber(int a)
	{
		num1 = a;
	}
	
	public void setSecondNumber(int b)
	{
		num2 = b;
	}
	
	public int additionOfTwoNumbers()
	{
		return num1+num2;
	}
}
