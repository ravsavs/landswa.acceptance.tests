package com.landswa.acceptance.tests;

import com.landswa.enums.ContactMethod;
import com.landswa.enums.Decision;
import com.landswa.enums.User;
import com.landswa.helper.Constants;
import com.landswa.poms.BasePage;
import com.landswa.poms.LoginPage;
import com.landswa.poms.MyDashboardPage;

public class CaseSubmission extends SetupTeardown {
	LoginPage loginPage;
    MyDashboardPage myDashboard;
    
	public void setupSelenium()
	{
		TestSuiteSetup();
		SetUp();
        Driver.get(Constants.loginUrl);
	}
	
	public void teardownSelenium()
	{
		TestTeardown();
	}
	
	public void userLogsInAsOfficerWithUsernameAndPassword(String username, String password) throws Exception
	{
        loginPage = new LoginPage(Driver);

        BasePage dashboard = loginPage.EnterUsername(username, User.Officer)
            .EnterPassword(password)
            .ClickLoginButton();

        myDashboard = (MyDashboardPage)dashboard;
	}

	public void createANewCaseWithApplicantAndRequestCategoryTypeAndLgaBeing(
			String applicantName,
	        String category,
	        String LGAName
	        ) throws Exception
	{
		myDashboard.ClickCreateNewCaseButon()
        .SearchAnApplicantWithName(applicantName)
        .SelectTheApplicantFromSearchResultWithName(applicantName)
        .Continue()
        .ClickContinueButton()
        .SelectGeneralRequestType()
        .ClickOnCategoryDropDown()
        .SelectFromDopDown(category)
        .EnterDescription("Automation Test")
        .ClickContinueButton()
        .AddLandRecordForLGA(LGAName)
        .ClickContinueButton()
        .ClickLGACheckboxToConsult()
        .HasLGABeenConsultedRadioButtonResponse(Decision.Yes)
        .ClickUpdateButton()
        .ClickContinueButton()
        .ClickContinueButton()
        .ClickCheckBox()
        .ClickContinueButton()
        .ClickTermsAndConditionsCheckbox()
        .ClickSignedCheckbox()
        .EnterDateReceived()
        .EnterDatesigned()
        .ClickSubmitButton()
        .SendCaseSummaryToApplicantRadioButton(Decision.Yes)
        .SelectMethodOfContact(ContactMethod.Email)
        .AnyOtherDocumentsToSend(Decision.No)
        .ClickReadyToEmailConfirmationCheckbox()
        .ClickDoneButton();
	}
    
}
