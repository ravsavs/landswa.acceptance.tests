package com.landswa.acceptance.tests;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;

import com.landswa.enums.BrowserType;

public class SetupTeardown {
	
    protected static WebDriver Driver;

    public void TestSuiteSetup()
    {
        Driver = GetLocalDriver(BrowserType.Chrome);
        Driver.manage().window().maximize();
    }

    public static void SetUp()
    {
        Driver.manage().deleteAllCookies();
    }

    public static void TearDown()
    {

    }

    public static void TestTeardown()
    {
        try
        {
            CleanUpInstances();
        }
        catch (Exception e)
        {
            System.out.println(e.getMessage());
        }
    }

    private static WebDriver GetLocalDriver(BrowserType browserType)
    {
        switch (browserType)
        {
            case Chrome:
                /**
                 * ChromeOptions options = new ChromeOptions();
                 * this path needs to be setup on TeamCity build agent as well - about:version
                 * options.AddArguments(@"user-data-dir=C:\Chromium\Temp\Profile\Default");
                 * driver = new ChromeDriver(options);
                 **/
        		String exePath = ".\\FitNesseRoot\\jar\\chromedriver.exe";
        		System.setProperty("webdriver.chrome.driver", exePath);
                //options.AddArguments(@"user-data-dir=C:\Chromium\Temp\Profile\Default");
                //Driver = new ChromeDriver(options);
                Driver = new ChromeDriver();
                break;
            case FireFox:
                Driver = new FirefoxDriver();
                break;
            case InternetExplorer:
                Driver = new InternetExplorerDriver();
                break;
        }
        return Driver;
    }

    public static void CleanUpInstances()
    {
        if (Driver != null)
        {
            Driver.close();
            Driver.quit();
            Driver = null;
        }
    }

}
