package com.landswa.acceptance.tests;

import org.junit.Before;
import org.junit.Test;

import fitnesse.junit.JUnitHelper;

public class TestRunner {

	JUnitHelper helper;
	@Before
	public void Prepare()
	{
		helper = new JUnitHelper(".", ".");
		helper.setPort(9090);
	}
	
	@Test
    public void restFixtureTest() throws Exception {
        helper.assertTestPasses("FitNesse.AcceptanceTestsSuite.UserLoginTestSuite.ManagerCanLoginToTeamDashboard");
		//helper.assertTestPasses("FitNesse.AcceptanceTestsSuite.CaseSubmissionFlowTestSuite.VerifyThatAnOfficerIsAbleToCreateAndSubmitANewCase");
    }

}
