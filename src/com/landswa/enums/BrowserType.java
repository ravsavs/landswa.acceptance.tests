package com.landswa.enums;

public enum BrowserType
{
    Chrome,
    FireFox,
    InternetExplorer
}
