package com.landswa.enums;

public enum ContactMethod {
    Email,
    PrintAndPost,
    BothEmailAndPrintAndPost

}
