package com.landswa.enums;

public enum User {

        Officer,
        Manager,
        ExecutiveDirector
}
